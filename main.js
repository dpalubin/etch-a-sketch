function emptyContainer(){
  let container = document.querySelector('.container');

  while(container.hasChildNodes()){
    let row = container.firstChild;
    while(row.hasChildNodes()){
      row.removeChild(row.firstChild);
    }
    container.removeChild(row);
  }
}

function fillContainer(resolution){
  let container = document.querySelector('.container');
  
  for(let i = 0; i < resolution; i++){
    let row = document.createElement('div');
    row.classList.add('row');
    for(let j = 0; j < resolution; j++){
      let pixel = document.createElement('div');
      pixel.classList.add('pixel');
      pixel.addEventListener('mouseover', changeColor);
      row.appendChild(pixel);
    }
    container.appendChild(row);
  }
}

function changeColor(e){
  let options = document.querySelector('select');
  let colorString;
  let rgb;

  switch(options.value){
    case "black":
      colorString = this.style['background-color'];
      rgb = colorString.slice(4,-1).split(',');

      if(rgb.every((color) => parseInt(color) === 255) || rgb.length != 3){
        this.style['background-color'] = 'rgb(0, 0, 0)';
      }
      else{
        this.style['background-color'] = 'rgb(255, 255, 255';
      }
      break;
    case "random":
      let red = Math.random()*255;
      let green = Math.random()*255;
      let blue = Math.random()*255;
      this.style['background-color'] = `rgb(${red}, ${green}, ${blue})`;
      break;
    case "gradual":
      colorString = this.style['background-color'];
      rgb = colorString.slice(4,-1).split(',');
      if(!rgb || rgb.length != 3){
        rgb= 'rgb(245, 245, 245)';
      }
      else{
        rgb = rgb.map(function (color) {
          color -= 10;
          if(color < 0) color = 0;
          return color;
        });
        rgb = `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`;
      }
      this.style['background-color'] = rgb;
      break;
    default:
      break;
  }
}

function setResolution(e){
  let resolution = parseInt(prompt('Enter the number of "pixels" per row and column (1 to 100):', 16));
  
  if(!resolution || resolution > 100 || resolution < 1) resolution = 16;

  emptyContainer();
  fillContainer(resolution);
}

let button = document.querySelector('button');


button.addEventListener('click', setResolution);

fillContainer(16);