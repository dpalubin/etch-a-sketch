# Etch-A-Sketch

This project was created as part of The Odin Project (TOP) curriculum.  The project assignment page can be found [here] (https://www.theodinproject.com/lessons/foundations-etch-a-sketch).

## License

This software is provided under the MIT license, Copyright 2023 David Palubin.
